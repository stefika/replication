### Enumeration Experiment, first run (students), second analysis
## Design
# Repeated measures/Mixed measures
## Controlled Factors:
# Task (Task0, Task1, Task2) - no warmup
# Lang (1[constants], 2[enums])
## Random Factors:
# Level of Education (Freshman[1], Sophomore[2], Junior[3], Senior[4], Graduate[5], Post-Graduate[6])
# Other data available in survey

setwd("~/Google Drive/Enum EPI Study/First Run (Students)/RWorkspace/")
d <- read.csv ("enumR1resultstime.csv")

library(reshape2)
library(ez)
library(ggplot2)
library(psych)

# Take out the ids which didn't behave correctly
# Didn't follow rules
# Had bugs
# Gave up and didn't make real attempts at tasks
d <- subset(d, id != 29 & id != 49 & id != 55 & id != 91 & id != 110 & id != 30 & id != 58 & id != 79 & id != 87 & id != 98 )

describe(d)
#        vars  n    mean     sd median trimmed    mad  min  max range  skew kurtosis    se
#id         1 57   69.18  23.36     70   69.13  26.69   27  109    82  0.02    -1.13  3.09
#year*      2 57    4.07   1.27      4    4.04   1.48    1    6     5  0.03    -1.18  0.17
#yearnum    3 57    3.30   0.93      3    3.28   1.48    1    6     5  0.46     1.08  0.12
#lang       4 57    1.49   0.50      1    1.49   0.00    1    2     1  0.03    -2.03  0.07
#Task0      5 57  893.02 592.15    621  825.02 312.83  278 2104  1826  1.12    -0.22 78.43
#Task1      6 57 1681.12 461.61   1708 1728.66 582.66  682 2120  1438 -0.57    -1.08 61.14
#Task2      7 57 2058.49 183.74   2101 2101.30   0.00 1164 2103   939 -4.05    15.04 24.34
summary(d)
#       id                    year       yearnum           lang           Task0          Task1          Task2     
# Min.   : 27.00   Freshman     : 1   Min.   :1.000   Min.   :1.000   Min.   : 278   Min.   : 682   Min.   :1164  
# 1st Qu.: 50.00   Graduate     : 1   1st Qu.:3.000   1st Qu.:1.000   1st Qu.: 460   1st Qu.:1307   1st Qu.:2101  
# Median : 70.00   Junior       :26   Median :3.000   Median :1.000   Median : 621   Median :1708   Median :2101  
# Mean   : 69.18   Post-graduate: 2   Mean   :3.298   Mean   :1.491   Mean   : 893   Mean   :1681   Mean   :2058  
# 3rd Qu.: 86.00   Senior       :19   3rd Qu.:4.000   3rd Qu.:2.000   3rd Qu.:1100   3rd Qu.:2101   3rd Qu.:2102  
# Max.   :109.00   Sophomore    : 8   Max.   :6.000   Max.   :2.000   Max.   :2104   Max.   :2120   Max.   :2103 

d$year <- factor(d$year, levels=c("Freshman", "Sophomore", "Junior", "Senior", "Graduate", "Post-graduate"))

constantsonly <- subset(d, lang == 1)
describe(constantsonly)
#        vars  n    mean     sd median trimmed    mad  min  max range  skew kurtosis     se
#id         1 29   66.52  23.37     69   66.40  28.17   27  106    79  0.06    -1.13   4.34
#year*      2 29    3.28   1.03      3    3.24   1.48    1    6     5  0.21     0.25   0.19
#yearnum    3 29    3.28   1.03      3    3.24   1.48    1    6     5  0.21     0.25   0.19
#lang       4 29    1.00   0.00      1    1.00   0.00    1    1     0   NaN      NaN   0.00
#Task0      5 29  939.07 615.95    678  897.76 397.34  278 2104  1826  0.92    -0.65 114.38
#Task1      6 29 1557.69 474.27   1599 1580.20 744.27  683 2105  1422 -0.16    -1.41  88.07
#Task2      7 29 2079.31 118.92   2101 2101.28   0.00 1461 2103   642 -4.84    22.20  22.08

enumsonly <- subset(d, lang == 2)
describe(enumsonly)
#        vars  n    mean     sd median trimmed    mad  min  max range  skew kurtosis     se
#id         1 28   71.93  23.46   73.5   72.00  26.69   32  109    77 -0.02    -1.28   4.43
#year*      2 28    3.32   0.82    3.0    3.29   0.00    2    6     4  0.95     1.98   0.15
#yearnum    3 28    3.32   0.82    3.0    3.29   0.00    2    6     4  0.95     1.98   0.15
#lang       4 28    2.00   0.00    2.0    2.00   0.00    2    2     0   NaN      NaN   0.00
#Task0      5 28  845.32 573.73  618.0  784.25 273.54  298 2101  1803  1.30     0.15 108.43
#Task1      6 28 1808.96 418.78 2100.5 1862.54   5.19  682 2120  1438 -1.07    -0.13  79.14
#Task2      7 28 2036.93 233.26 2101.0 2100.21   0.74 1164 2102   938 -3.17     8.38  44.08

dd <- melt(d, id.vars=c("id", "year", "yearnum", "lang"))
colnames(dd)[6] <- "time"
colnames(dd)[5] <- "task"



# Sort year by chronological progression
dd$year <- factor(dd$year, levels=c("Freshman", "Sophomore", "Junior", "Senior", "Graduate", "Post-graduate"))

dd$lang <- factor(dd$lang, levels=c("1","2"), labels=c("constants", "enums"))
dd$id <- as.factor(dd$id)

dd.constantsonly <- subset(dd, lang == "constants")
describe(dd.constantsonly)
#        vars  n    mean     sd median trimmed    mad min  max range  skew kurtosis    se
#id*        1 87   27.03  16.20     28   26.90  20.76   1   54    53  0.04    -1.18  1.74
#year*      2 87    3.28   1.02      3    3.25   1.48   1    6     5  0.22     0.41  0.11
#yearnum    3 87    3.28   1.02      3    3.25   1.48   1    6     5  0.22     0.41  0.11
#lang*      4 87    1.00   0.00      1    1.00   0.00   1    1     0   NaN      NaN  0.00
#task*      5 87    2.00   0.82      2    2.00   1.48   1    3     2  0.00    -1.53  0.09
#time       6 87 1525.36 648.92   1708 1587.63 584.14 278 2105  1827 -0.54    -1.32 69.57

dd.enumsonly <- subset(dd, lang == "enums")
describe(dd.enumsonly)
#        vars  n    mean     sd median trimmed   mad min  max range  skew kurtosis    se
#id*        1 84   31.04  16.66   32.5   31.13 20.76   3   57    54 -0.06    -1.32  1.82
#year*      2 84    3.32   0.81    3.0    3.31  0.00   2    6     4  0.99     2.23  0.09
#yearnum    3 84    3.32   0.81    3.0    3.31  0.00   2    6     4  0.99     2.23  0.09
#lang*      4 84    2.00   0.00    2.0    2.00  0.00   2    2     0   NaN      NaN  0.00
#task*      5 84    2.00   0.82    2.0    2.00  1.48   1    3     2  0.00    -1.54  0.09
#time       6 84 1563.74 672.13 2097.5 1636.60  9.64 298 2120  1822 -0.67    -1.30 73.34



demo <- read.csv("extrasurvey (3).csv")
demo2 = demo[!duplicated(demo$id), c("id", "gender")] # removes duplicates from data (we only care about demographics here and the duplicates are all about the language survey)

timewithdemo <- merge(d, demo2, by="id", all.x = TRUE)

describe(timewithdemo)
#        vars  n    mean     sd median trimmed    mad  min  max range  skew kurtosis    se
#id         1 57   69.18  23.36     70   69.13  26.69   27  109    82  0.02    -1.13  3.09
#year*      2 57    3.30   0.93      3    3.28   1.48    1    6     5  0.46     1.08  0.12
#yearnum    3 57    3.30   0.93      3    3.28   1.48    1    6     5  0.46     1.08  0.12
#lang       4 57    1.49   0.50      1    1.49   0.00    1    2     1  0.03    -2.03  0.07
#Task0      5 57  893.02 592.15    621  825.02 312.83  278 2104  1826  1.12    -0.22 78.43
#Task1      6 57 1681.12 461.61   1708 1728.66 582.66  682 2120  1438 -0.57    -1.08 61.14
#Task2      7 57 2058.49 183.74   2101 2101.30   0.00 1164 2103   939 -4.05    15.04 24.34
#gender*    8 38    1.95   0.46      2    1.94   0.00    1    3     2 -0.20     1.46  0.07
summary(timewithdemo)
#       id                    year       yearnum           lang           Task0          Task1          Task2     
# Min.   : 27.00   Freshman     : 1   Min.   :1.000   Min.   :1.000   Min.   : 278   Min.   : 682   Min.   :1164  
# 1st Qu.: 50.00   Sophomore    : 8   1st Qu.:3.000   1st Qu.:1.000   1st Qu.: 460   1st Qu.:1307   1st Qu.:2101  
# Median : 70.00   Junior       :26   Median :3.000   Median :1.000   Median : 621   Median :1708   Median :2101  
# Mean   : 69.18   Senior       :19   Mean   :3.298   Mean   :1.491   Mean   : 893   Mean   :1681   Mean   :2058  
# 3rd Qu.: 86.00   Graduate     : 1   3rd Qu.:4.000   3rd Qu.:2.000   3rd Qu.:1100   3rd Qu.:2101   3rd Qu.:2102  
# Max.   :109.00   Post-graduate: 2   Max.   :6.000   Max.   :2.000   Max.   :2104   Max.   :2120   Max.   :2103  
#    gender  
# Female: 5  
# Male  :30  
# NULL  : 3  
# NA's  :19  

model <- ezANOVA(data=dd, dv= .(time), wid= .(id), between=.(lang), within=.(task), detailed=TRUE, type=3)
model
#$ANOVA
#       Effect DFn DFd          SSn      SSd            F            p p<.05         ges
#1 (Intercept)   1  55 407816500.31 17845462 1256.8970259 1.433781e-39     * 0.926381515
#2        lang   1  55     62958.23 17845462    0.1940383 6.613031e-01       0.001938866
#3        task   2 110  40376595.99 14563252  152.4874272 1.929300e-32     * 0.554735515
#4   lang:task   2 110    987281.94 14563252    3.7285977 2.711554e-02     * 0.029562884
#
#$`Mauchly's Test for Sphericity`
#     Effect         W          p p<.05
#3      task 0.8755943 0.02768086     *
#4 lang:task 0.8755943 0.02768086     *
#
#$`Sphericity Corrections`
#     Effect       GGe        p[GG] p[GG]<.05       HFe        p[HF] p[HF]<.05
#3      task 0.8893587 3.882191e-29         * 0.9170033 5.800145e-30         *
#4 lang:task 0.8893587 3.220972e-02         * 0.9170033 3.085304e-02         *

tasktest <- pairwise.t.test(dd$time, dd$task, paired=TRUE, p.adjust.method = "bonferroni")
tasktest
#	Pairwise comparisons using paired t tests 
#
#data:  dd$time and dd$task 
#
#      Task0   Task1  
#Task1 3.0e-15 -      
#Task2 < 2e-16 1.4e-07
#
#P value adjustment method: bonferroni 

design <- ezDesign(dd, lang, year) # shows we have not enough balanced data in Freshman and Graduate years, so we have to take them out
dd.FilteredYear <- subset(dd, year != "Freshman" & year != "Graduate") # There is only one freshman and including them in the data causes an ANOVA error because there is data missing
model2 <- ezANOVA(data=dd.FilteredYear, dv= .(time), wid= .(id), between=.(lang, year), within=.(task), detailed=TRUE, type=3) # Testing to see the year impact
model2
#$ANOVA
#          Effect DFn DFd          SSn      SSd           F            p p<.05         ges
#1    (Intercept)   1  47 175600327.43 15988316 516.2029076 5.437194e-27     * 0.861516788
#2           lang   1  47     74162.05 15988316   0.2180102 6.427175e-01       0.002620496
#3           year   3  47    771877.35 15988316   0.7563489 5.242586e-01       0.026617850
#5           task   2  94  13035218.28 12238292  50.0605183 1.576147e-15     * 0.315914714
#4      lang:year   3  47    803997.14 15988316   0.7878225 5.067642e-01       0.027694811
#6      lang:task   2  94    550311.17 12238292   2.1134178 1.265295e-01       0.019123352
#7      year:task   6  94    328752.07 12238292   0.4208470 8.634780e-01       0.011512797
#8 lang:year:task   6  94   1633818.75 12238292   2.0915086 6.140306e-02       0.054715183
#
#$`Mauchly's Test for Sphericity`
#          Effect         W          p p<.05
#5           task 0.8750438 0.04641786     *
#6      lang:task 0.8750438 0.04641786     *
#7      year:task 0.8750438 0.04641786     *
#8 lang:year:task 0.8750438 0.04641786     *
#
#$`Sphericity Corrections`
#          Effect       GGe        p[GG] p[GG]<.05       HFe        p[HF] p[HF]<.05
#5           task 0.8889235 4.566965e-14         * 0.9214141 1.705216e-14         *
#6      lang:task 0.8889235 1.326772e-01           0.9214141 1.308698e-01          
#7      year:task 0.8889235 8.439638e-01           0.9214141 8.500093e-01          
#8 lang:year:task 0.8889235 7.026736e-02           0.9214141 6.753891e-02          

graph1 <- ggplot(dd, aes(x=task , y=time, fill=lang)) + geom_boxplot() + labs (x="Task", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + coord_cartesian (ylim=c(0, 2200)) + scale_y_continuous(breaks=seq(0,2200,500), expand = c(0,0)) + theme(legend.position = "top", panel.grid.major = element_blank(), panel.grid.minor = element_blank(), panel.background = element_blank(), axis.line = element_line(colour = "black")) + scale_fill_grey(start = 1.0, end = 0.75)

graph1

ggsave (file = paste("boxplotTime", '.eps', sep= ""), graph1)
ggsave (file = paste("boxplotTime", '.pdf', sep= ""), graph1)

graph2 <- ggplot(dd, aes(x=task , y=time, fill=lang)) + geom_boxplot() + labs (x="Task", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + coord_cartesian (ylim=c(0, 2200)) + scale_y_continuous(breaks=seq(0,2200,500), expand = c(0,0)) + theme(legend.position = "top", panel.grid.major = element_blank(), panel.grid.minor = element_blank(), panel.background = element_blank(), axis.line = element_line(colour = "black")) + scale_fill_grey(start = 1.0, end = 0.75) + facet_grid(year ~ .)

graph2

ggsave (file = paste("boxplotTimeByClass", '.eps', sep= ""), graph2)
ggsave (file = paste("boxplotTimeByClass", '.pdf', sep= ""), graph2)

yearcount <- ggplot (d, aes(x=year)) + geom_bar()
ggsave (file = paste("yearcount", '.pdf', sep= ""), yearcount)
ggsave (file = paste("yearcount", '.eps', sep= ""), yearcount)


# Investigation of fileswitches
fileswitches <- read.csv("fileswitches.csv")

fileswitches$lang <- factor(fileswitches$lang, levels=c("1","2"), labels=c("constants", "enums"))
fileswitches$task <- factor(fileswitches$task, levels=c("0","1","2"), labels=c("Task 0", "Task 1", "Task 2"))
fileswitch.filtered <- subset(fileswitches, id != 29 & id != 49 & id != 55 & id != 91 & id != 110 & id != 30 & id != 58 & id != 79 & id != 87 & id != 98 )
# Merge with all flag, remove bogus entries without ID, set missing entries to 0
dd.fs <- merge (dd, fileswitch.filtered, by=c("id","task","lang"), all = TRUE)
dd.fs <- subset(dd.fs, !is.na(dd.fs$id))
dd.fs[is.na(dd.fs)] <- 0

fileswitchLineGraph <- ggplot(dd.fs, aes(y=fileswitch, x=time, group = lang, color = lang)) + geom_point() + geom_smooth(method=lm, se=FALSE)
fileswitchLineGraph

model.fs <- ezANOVA(data=dd.fs, dv= .(fileswitch), wid= .(id), between=.(lang), within=.(task), detailed=TRUE, type=3)
model.fs
$ANOVA
#       Effect DFn DFd         SSn      SSd          F            p p<.05        ges
#1 (Intercept)   1  55 12173.24035 2248.105 297.818970 7.370284e-24     * 0.62285270
#2        lang   1  55    75.55614 2248.105   1.848485 1.795078e-01       0.01014633
#3        task   2 110  1333.98308 5122.988  14.321539 2.966177e-06     * 0.15324199
#4   lang:task   2 110   303.24623 5122.988   3.255628 4.230230e-02     * 0.03951432
#
#$`Mauchly's Test for Sphericity`
#     Effect         W            p p<.05
#3      task 0.6897484 4.412142e-05     *
#4 lang:task 0.6897484 4.412142e-05     *
#
#$`Sphericity Corrections`
#     Effect       GGe        p[GG] p[GG]<.05       HFe        p[HF] p[HF]<.05
#3      task 0.7632122 2.934034e-05         * 0.7805703 2.478959e-05         *
#4 lang:task 0.7632122 5.631972e-02           0.7805703 5.515659e-02          

tasktest.fs <- pairwise.t.test(dd.fs$fileswitch, dd.fs$task, paired=TRUE, p.adjust.method = "bonferroni")
tasktest.fs

#	Pairwise comparisons using paired t tests 
#
#data:  dd.fs$fileswitch and dd.fs$task 
#
#      Task0   Task1  
#Task1 2.6e-05 -      
#Task2 0.00012 0.33345
#
#P value adjustment method: bonferroni 

cor.test(dd.fs$fileswitch, dd.fs$time, method= "pearson", conf.level= 0.95)
#	Pearson's product-moment correlation
#
#data:  dd.fs$fileswitch and dd.fs$time
#t = 6.6453, df = 162, p-value = 4.387e-10
#alternative hypothesis: true correlation is not equal to 0
#95 percent confidence interval:
# 0.3332011 0.5752662
#sample estimates:
#      cor 
#0.4628181 