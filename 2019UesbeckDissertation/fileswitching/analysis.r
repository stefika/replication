install.packages(c("tidyverse", "car", "compute.es", "multcomp", "pastecs", "WRS2", "psych", "gmodels"))

library(car)
#library(compute.es)
#library(multcomp)
#library(pastecs)
library(WRS2)
library(tidyverse)
library(ez)
library(psych)
library(gmodels)

#setwd("~/Google Drive File Stream/My Drive/Lab/Studies/FileSwitch")
setwd("K:/My Drive/Lab/Studies/FileSwitch")
d <- read.csv("times.csv")
errors <- read.csv("errorcount.csv")

d <- d %>% left_join(errors, by= c("id", "task", "lang"))
d <- d %>% replace_na(list(errorcount = 0))


nrow(d)



# Count of unique participants
d %>% summarize(count=n_distinct(id))

str(d)

names(d) <- tolower(names(d))

d <- d %>% replace_na(list(year = "Professional"))
d$year <- factor(d$year, levels=c("Freshman", "Sophomore", "Junior", "Senior", "Graduate", "Not Applicable", "Professional"), 
                 labels=c("Freshman", "Sophomore", "Junior", "Senior", "Graduate", "Not Applicable", "Professional"))






d %>% group_by(naturallanguage) %>% summarize(n=n_distinct(id))

d <- d %>% mutate(notenglish = if_else(naturallanguage == "English", 0 , 1))
d$notenglish <- factor(d$notenglish, levels = c(1, 0), labels=c("No","Yes"))
d <- d %>% mutate(fluency = replace(fluency, fluency == -1, 10))
d %>% group_by(notenglish) %>% summarize(n=n_distinct(id))

d %>% group_by(fluency) %>% summarize(n=n_distinct(id))


ezDesign(d, x=id, y=task)

incomplete.ids <- d %>% group_by(id) %>% 
  summarize(sumtasknum = sum(task)) %>%
  filter(sumtasknum < 6) %>% 
  select(id) %>%
  pull(id)

incomplete.ids
length(incomplete.ids)

d <- d %>%
  filter (!(id %in% incomplete.ids))

nrow(d)

# Count of unique participants
d %>% summarize(count=n_distinct(id))

d %>% group_by(lang) %>%
  summarize(count = n_distinct(id))

d %>% group_by(year) %>%
  summarize(count=n_distinct(id))

d %>% group_by(year, lang) %>%
  summarize(count=n_distinct(id))


d <- d %>% filter(year == "Sophomore" | year == "Junior" | year == "Senior")

d %>% summarize(count=n_distinct(id))

d %>% group_by(lang) %>%
  summarize(count = n_distinct(id))

d %>% group_by(year) %>%
  summarize(count=n_distinct(id))

d %>% group_by(year, lang) %>%
  summarize(count=n_distinct(id))


ezDesign(d, x=id, y=task)

d <- d %>% mutate(time = replace(time, time > 3600, 3600))

d.alt <- d %>% filter(gaveup != 1)




d <- d %>% mutate(time = replace(time, gaveup == 1, 3600))




d$lang <- factor(d$lang, levels=c(1, 2, 3),
                 labels=c("Polyglot", "JavaScript", "PHP"))

d$task <- factor(d$task, levels=c("0","1","2","3"), 
                 labels=c("1","2","3", "4"))

d.alt$task <- factor(d.alt$task, levels=c("0","1","2","3"), 
                 labels=c("1","2","3", "4"))
d.alt$lang <- factor(d.alt$lang, levels=c(1, 2, 3),
                 labels=c("Polyglot", "JavaScript", "PHP"))

d$totalxp <- as.numeric(d$totalxp)

d$id <- factor(d$id)

d %>% group_by(notenglish) %>% summarize(n=n_distinct(id))

d %>% group_by(naturallanguage) %>% summarize(n=n_distinct(id))

d <- d %>% mutate(notenglish = if_else(naturallanguage == "English", 0 , 1))
d$notenglish <- factor(d$notenglish, levels = c(1, 0), labels=c("No","Yes"))

# Numbers in years
d %>% group_by(year) %>%
  summarize(count=n_distinct(id))

# Numbers by programming experience
d %>% group_by(totalxp) %>%
  summarize(count=n_distinct(id))

# Numbers by job experience
d %>% group_by(jobxp) %>%
  summarize(count=n_distinct(id))

# Numbers in groups
d %>% group_by(lang) %>%
  summarize(count=n_distinct(id))

# Numbers in genders
d %>% group_by(gender) %>% 
  summarize(count=n_distinct(id))

# Average age
d %>% summarize(avgage=mean(age), sd = sd(age))

ggplot(d, aes(x=time)) + geom_histogram()

d.onlymeat <- d %>% filter(task == 3 | task == 4)

d.alt.onlymeat <- d.alt %>% filter(task == 3 | task == 4)

nrow(d.onlymeat)
  
ezDesign(d.onlymeat, x=year, y=lang)

###########################################################################################################################################
#Time
###########################################################################################################################################

options(contrasts=c("contr.sum","contr.poly"))

model <- ezANOVA(data=d.onlymeat, dv= .(time), wid= .(id), between=.(lang, year), within = .(task), observed=.(year), detailed=TRUE, type=3)
model

pairwise.t.test(d.onlymeat$time, d.onlymeat$lang, paired = FALSE, p.adjust.method= "bonferroni")


d.onlymeat %>% group_by(lang) %>% 
  summarize(n=n_distinct(id), mean=mean(time), sd=sd(time)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(lang, task) %>% 
  summarize(n=n_distinct(id), mean=mean(time), sd=sd(time)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(task, lang) %>% 
  summarize(n=n_distinct(id), mean=mean(time), sd=sd(time)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(task) %>% 
  summarize(n=n_distinct(id), mean=mean(time), sd=sd(time)) %>% mutate_if(is.numeric, format, 1)

graph.general <- ggplot(d.onlymeat, aes(x=lang , y=time)) +
  geom_boxplot() +
  labs (x="Group", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600))
graph.general

ggsave("fs-general.eps")
ggsave("fs-general.png")

graph2 <- ggplot(d.onlymeat, aes(x=lang , y=time, fill=year)) +
  geom_boxplot() +
  labs (x="Group", y="Time [s]", group="Level of Education", linetype="Level of Education", fill="Level of Education" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600))
graph2

ggsave("fs-breakdown.eps")
ggsave("fs-breakdown.png")

graph3 <- ggplot(d.onlymeat, aes(x=task , y=time, fill=lang)) +
  geom_boxplot() +
  labs (x="Task", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600))
graph3

ggsave("fs-bytask.eps")
ggsave("fs-bytask.png")


graph.general <- ggplot(d.onlymeat, aes(x=task, y=time, fill=lang)) +
  geom_boxplot() +
  labs (x="Group", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600)) + facet_grid(notenglish ~ year) 
graph.general

ggsave("fs-allthethings.png")

###########################################################################################################################################
#Language
###########################################################################################################################################
test <- t.test(time ~ notenglish, data = d.onlymeat, paired = FALSE)
test

d.onlypolyglot <- d.onlymeat %>% filter(lang == "Polyglot")
test.2 <- t.test(time ~ notenglish, data = d.onlypolyglot, paired = FALSE)
test.2


model <- ezANOVA(data=d.onlymeat, dv= .(time), wid= .(id), between=.(lang, notenglish), observed=.(notenglish), detailed=TRUE, type=3)
model

d.onlymeat %>% group_by(notenglish) %>%
  summarize(n=n(), mean=mean(time), sd=sd(time)) %>% 
              mutate_if(is.numeric, format, 1)



t.23 <- test$statistic[[1]]
df.23 <- test$parameter[[1]]

t.23
df.23

r <- sqrt(t.23^2/(t.23^2+df.23))

round(r, 3)
round(r*r, 4)

Dgraph.notenglish <- ggplot(d.onlymeat, aes(x=notenglish , y=time)) + 
  geom_boxplot() + 
  labs (x="English as Primary Language", y="Time [s]", group="Database Experience", 
        linetype="Database Experience", fill="Database Experience" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(), panel.background = element_blank(), 
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) + 
  scale_fill_grey(start = 1.0, end = 0.75) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600))
graph.notenglish

ggsave("fs-notenglish.eps")


graph.notenglish <- ggplot(d.onlymeat, aes(x=notenglish , y=time, fill=lang)) + 
  geom_boxplot() + 
  labs (x="English as Primary Language", y="Time [s]", group="Database Experience", 
        linetype="Database Experience", fill="Database Experience" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(), panel.background = element_blank(), 
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) + 
  scale_fill_grey(start = 1.0, end = 0.75) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600))
graph.notenglish

ggsave("fs-notenglish-group.png")

ggplot(d.onlymeat, aes(x=fluency)) + geom_histogram()
ggplot(d.onlymeat, aes(x=time)) + geom_histogram()
cor.test( d.onlymeat$time, d.onlymeat$fluency, method= "kendall")

model <- ezANOVA(data=d.onlymeat, dv= .(time), wid= .(id), between=.(fluency), observed=.(fluency), detailed=TRUE, type=3)
model
graph.fluency.4 <- ggplot(d.onlymeat, aes(x=fluency, y=time, color=notenglish)) + geom_point()
graph.fluency.4

###########################################################################################################################################
#Errors
###########################################################################################################################################
d.onlymeat$errorcount
model <- ezANOVA(data=d.onlymeat, dv= .(errorcount), wid= .(id), between=.(lang), detailed=TRUE, type=3)
model

d.onlymeat %>% 
  summarize(n=n_distinct(id), mean=mean(errorcount), sd=sd(errorcount), min = min(errorcount), max=max(errorcount)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(lang) %>% 
  summarize(n=n_distinct(id), mean=mean(errorcount), sd=sd(errorcount)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(lang, task) %>% 
  summarize(n=n_distinct(id), mean=mean(errorcount), sd=sd(errorcount)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(task, lang) %>% 
  summarize(n=n_distinct(id), mean=mean(errorcount), sd=sd(errorcount)) %>% mutate_if(is.numeric, format, 1)

d.onlymeat %>% group_by(task) %>% 
  summarize(n=n_distinct(id), mean=mean(errorcount), sd=sd(errorcount)) %>% mutate_if(is.numeric, format, 1)


d.onlymeat %>% 
  summarize(ci = list(mean_cl_normal(errorcount) %>% 
                        rename(mean=y, lwr=ymin, upr=ymax))) %>% 
  unnest


graph.general <- ggplot(d.onlymeat, aes(x=lang , y=errorcount)) +
  geom_boxplot() +
  labs (x="Group", y="Number of Errors", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,10,20,30,40,50),limits = c(0, 50))
graph.general

ggsave("fs-general-errors.eps")
ggsave("fs-general-errors.png")

graph2 <- ggplot(d.onlymeat, aes(x=lang , y=errorcount, fill=year)) +
  geom_boxplot() +
  labs (x="Group", y="Number of Errors", group="Level of Education", linetype="Level of Education", fill="Level of Education" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,10,20,30,40,50),limits = c(0, 50))
graph2

ggsave("fs-breakdown-errors.eps")
ggsave("fs-breakdown-errors.png")

graph3 <- ggplot(d.onlymeat, aes(x=task , y=errorcount, fill=lang)) +
  geom_boxplot() +
  labs (x="Task", y="Number of Errors", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,10,20,30,40,50),limits = c(0, 50))
graph3

ggsave("fs-bytask-errors.eps")
ggsave("fs-bytask-errors.png")

pairwise.t.test(d.onlymeat$errorcount, d.onlymeat$lang, paired = FALSE, p.adjust.method= "bonferroni")


ggplot(d.onlymeat, aes(x=errorcount)) + geom_histogram()
ggplot(d.onlymeat, aes(x=time)) + geom_histogram()
cor.test( d.onlymeat$time, d.onlymeat$errorcount, method= "kendall")


d.onlymeat.poly <- d.onlymeat %>% filter(lang == "Polyglot")
cor.test( d.onlymeat.poly$time, d.onlymeat.poly$errorcount, method= "kendall")

d.onlymeat.notpoly <- d.onlymeat %>% filter(lang != "Polyglot")
cor.test( d.onlymeat.notpoly$time, d.onlymeat.notpoly$errorcount, method= "kendall")

graph.errors.4 <- ggplot(d.onlymeat, aes(x=errorcount, y=time)) + geom_point()
graph.errors.4

###########################################################################################################################################
#Switches
###########################################################################################################################################
p.switches <- read.csv("switches.csv")

p.switches$id <- factor(p.switches$id)
p.switches$task <- factor(p.switches$task, levels=c("0","1","2","3"), 
                          labels=c("1","2","3", "4"))

d.alt.onlymeat$id <- factor(d.alt.onlymeat$id)
d.switches <- d.alt.onlymeat %>% left_join(p.switches, by= c("id", "task"))
d.switches <- d.switches %>% replace_na(list(switches = 0))

nrow(d.switches)
nrow(d.onlymeat)

d.switches <- d.switches %>% filter(switches != 0)

nrow(d.switches)

d.switches$switches

d.switches %>% group_by(lang) %>% 
  summarize(n=n_distinct(id), mean=mean(switches), sd=sd(switches), min=min(switches), max=max(switches)) %>% mutate_if(is.numeric, format, 1)

d.switches %>% 
  summarize(n=n_distinct(id), mean=mean(switches), sd=sd(switches)) %>% mutate_if(is.numeric, format, 1)

d.switches %>% group_by(task) %>%
  summarize(n=n_distinct(id), mean=mean(switches), sd=sd(switches)) %>% mutate_if(is.numeric, format, 1)

d.switches %>% group_by(lang, task) %>%
  summarize(n=n_distinct(id), mean=mean(switches), sd=sd(switches)) %>% mutate_if(is.numeric, format, 1)

d.switches %>% group_by(lang, year) %>%
  summarize(n=n_distinct(id), mean=mean(switches), sd=sd(switches)) %>% mutate_if(is.numeric, format, 1)

graph.switches.general <- ggplot(d.switches, aes(x=lang , y=switches)) +
  geom_boxplot() +
  labs (x="Group", y="Number of Switches", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,10,20,30),limits = c(0, 30))
graph.switches.general

ggsave("fs-general-switches.eps")
ggsave("fs-general-switches.png")

graph.switches.2 <- ggplot(d.switches, aes(x=lang , y=switches, fill=year)) +
  geom_boxplot() +
  labs (x="Group", y="Number of Switches", group="Level of Education", linetype="Level of Education", fill="Level of Education" ) +  
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,10,20,30),limits = c(0, 30))
graph.switches.2

ggsave("fs-breakdown-switches.eps")
ggsave("fs-breakdown-switches.png")

graph.switches.3 <- ggplot(d.switches, aes(x=task , y=switches, fill=lang)) +
  geom_boxplot() +
  labs (x="Task", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), panel.background = element_blank(),
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) +
  scale_fill_grey(start = 1.0, end = 0.75) +
  # ylim(0, 2700) +
  scale_y_continuous(breaks=c(0,10,20,30),limits = c(0, 30))
graph.switches.3

ggsave("fs-bytask-switches.eps")
ggsave("fs-bytask-switches.png")


model.switches <- ezANOVA(data=d.switches, dv= .(switches), wid= .(id), between=.(lang), detailed=TRUE, type=3)
model.switches


ggplot(d.switches, aes(x=switches)) + geom_histogram()
ggplot(d.onlymeat, aes(x=time)) + geom_histogram()
cor.test(d.switches$switches, d.switches$time, method= "kendall")

d.switches.poly <- d.switches %>% filter(lang == "Polyglot")
cor.test( d.switches.poly$time, d.switches.poly$errorcount, method= "kendall")

d.switches.notpoly <- d.switches %>% filter(lang != "Polyglot")
cor.test( d.switches.notpoly$time, d.switches.notpoly$errorcount, method= "kendall")


graph.switches.4 <- ggplot(d.switches, aes(x=switches, y=time, color=lang)) + geom_point()
graph.switches.4

graph.switches.4 <- ggplot(d.switches %>% filter(lang == "PHP"), aes(x=switches, y=time)) + geom_point()
graph.switches.4

graph.switches.4 <- ggplot(d.switches %>% filter(lang == "Polyglot"), aes(x=switches, y=time)) + geom_point()
graph.switches.4

pairwise.t.test(d.switches$switches, d.switches$lang, paired = FALSE, p.adjust.method= "bonferroni")

Model.lm <- lm (time ~ lang + errorcount + switches + year + task, data= d.switches)

summary(Model.lm)
###########################################################################################################################################
#Switch times
###########################################################################################################################################
df <- read.csv("raw_events.csv", colClasses=c("time"="character"))

df <- df %>% filter (task > 1) %>% arrange(id, task, time)

normal.d.ids <- d.onlymeat %>%
  select(id) %>%
  pull(id)

df <- df %>% filter (id %in% normal.d.ids)



switch.times <- data.frame(matrix(ncol = 6, nrow = 0))
colnames(switch.times) <- c("id" , "task", "lang", "time1", "time2", "timediff")

#typeof(df[1,"time"])

lastevent <- 0
for (row in 1:nrow(df)) {
  event <- df[row, "event"]
  output <- as.numeric(df[row, "output"])
  
  if (event == 27) {
    id   <- df[row, "id"]
    task <- df[row, "task"]
    lang <- df[row, "lang"]
    time <- df[row, "time"]
    lastevent <- 27
  }
  
  if (event == 24 && lastevent == 27 && output < 126 && output > 32) {
    if (id == df[row, "id"] && task == df[row, "task"]) {
      time2 <- df[row, "time"]
      t1 <- strptime( time, "%Y-%m-%d %H:%M:%S")
      t2 <- strptime( time2, "%Y-%m-%d %H:%M:%S")
      switch.times[nrow(switch.times) + 1,] = list(id, task, lang, time, time2, difftime(t2, t1, units="secs"))
    }
    lastevent <- 24
  }
  
}

#switch.times <- switch.times %>% group_by (id, task, lang) %>% summarize(timediff= first(timediff))
#
switch.times <-  switch.times %>% filter (timediff <= 500)



switch.times %>% group_by(lang) %>%
  summarize(n=n(), mean=mean(timediff), sd=sd(timediff), min=min(timediff), max=max(timediff))



model <- ezANOVA(data=switch.times, dv= .(timediff), wid= .(id), between=.(lang), detailed=TRUE, type=3)
model

ggplot (switch.times, aes(x=as.factor(lang), y=timediff)) + geom_boxplot()

pairwise.t.test(switch.times$timediff, switch.times$lang, paired = FALSE, p.adjust.method= "bonferroni")

###########################################################################################################################################
#CI graphs (those overwrite tidyverse libraries, use with caution)
###########################################################################################################################################
summarySE <- function(data=NULL, measurevar, groupvars=NULL, na.rm=FALSE,
                      conf.interval=.95, .drop=TRUE) {
  library(plyr)
  
  # New version of length which can handle NA's: if na.rm==T, don't count them
  length2 <- function (x, na.rm=FALSE) {
    if (na.rm) sum(!is.na(x))
    else       length(x)
  }
  
  # This does the summary. For each group's data frame, return a vector with
  # N, mean, and sd
  datac <- ddply(data, groupvars, .drop=.drop,
                 .fun = function(xx, col) {
                   c(N    = length2(xx[[col]], na.rm=na.rm),
                     mean = mean   (xx[[col]], na.rm=na.rm),
                     sd   = sd     (xx[[col]], na.rm=na.rm)
                   )
                 },
                 measurevar
  )
  
  # Rename the "mean" column    
  datac <- rename(datac, c("mean" = measurevar))
  
  datac$se <- datac$sd / sqrt(datac$N)  # Calculate standard error of the mean
  
  # Confidence interval multiplier for standard error
  # Calculate t-statistic for confidence interval: 
  # e.g., if conf.interval is .95, use .975 (above/below), and use df=N-1
  ciMult <- qt(conf.interval/2 + .5, datac$N-1)
  datac$ci <- datac$se * ciMult
  
  return(datac)
}

pd <- position_dodge(0.1)
p <- summarySE (d.onlymeat, measurevar = "time", groupvars = c("lang", "task"), na.rm=FALSE, conf.interval = .95)
p
ggplot(p, aes(x=task, y=time, colour=lang, group=lang)) + 
  labs (x="Task", y="Time [s]", group="Group", linetype="Group", fill="Group" ) + 
  geom_errorbar(aes(ymin=time-ci, ymax=time+ci), colour="black", width=.1, position=pd) +
  geom_line(position=pd) +
  geom_point(position=pd, size=3) +
  theme(legend.position = "bottom", panel.grid.major = element_blank(), 
      panel.grid.minor = element_blank(), panel.background = element_blank(), 
      text = element_text(size=20),
      axis.line = element_line(colour = "black")) + 
  scale_fill_grey(start = 1.0, end = 0.75) +
  scale_y_continuous(breaks=c(0,1000, 2000, 3000, 3600),limits = c(0, 3600))

ggsave("fs-ci.png")

p.sw <- summarySE (d.onlymeat, measurevar = "errorcount", groupvars = c("lang", "task"), na.rm=FALSE, conf.interval = .95)
p.sw
ggplot(p.sw, aes(x=task, y=errorcount, colour=lang, group=lang)) + 
  geom_errorbar(aes(ymin=errorcount-ci, ymax=errorcount+ci), colour="black", width=.1, position=pd) +
  geom_line(position=pd) +
  geom_point(position=pd, size=3) + 
  labs (x="Task", y="Number of Errors", group="Group", linetype="Group", fill="Group" ) + 
  theme(legend.position = "bottom", panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(), panel.background = element_blank(), 
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) + 
  scale_fill_grey(start = 1.0, end = 0.75) +
  scale_y_continuous(breaks=c(0,10,20,30,40,50),limits = c(0, 50))

ggsave("fs-ci-errors.png")

p.sw <- summarySE (d.switches, measurevar = "switches", groupvars = c("lang", "task"), na.rm=FALSE, conf.interval = .95)
p.sw
ggplot(p.sw, aes(x=task, y=switches, colour=lang, group=lang)) + 
  geom_errorbar(aes(ymin=switches-ci, ymax=switches+ci), colour="black", width=.1, position=pd) +
  geom_line(position=pd) +
  geom_point(position=pd, size=3) +
  labs (x="Task", y="Number of Switches", group="Group", linetype="Group", fill="Group" ) +
  theme(legend.position = "bottom", panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(), panel.background = element_blank(), 
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) + 
  scale_fill_grey(start = 1.0, end = 0.75) +
  scale_y_continuous(breaks=c(0,10,20),limits = c(0, 20))

ggsave("fs-ci-switches.png")


p.sw <- summarySE (switch.times, measurevar = "timediff", groupvars = c("lang", "task"), na.rm=FALSE, conf.interval = .95)
p.sw
ggplot(p.sw, aes(x=as.factor(task), y=timediff, colour=as.factor(lang), group=as.factor(lang))) + 
  geom_errorbar(aes(ymin=timediff-ci, ymax=timediff+ci), colour="black", width=.1, position=pd) +
  geom_line(position=pd) +
  geom_point(position=pd, size=3) +
  labs (x="Task", y="Number of Switches", group="Group", linetype="Group", fill="Group" ) +
  theme(legend.position = "bottom", panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(), panel.background = element_blank(), 
        text = element_text(size=20),
        axis.line = element_line(colour = "black")) + 
  scale_fill_grey(start = 1.0, end = 0.75) #+
 # scale_y_continuous(breaks=c(0,10,20),limits = c(0, 20))
