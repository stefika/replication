# README for replication packet for Randomized Controlled Trial on the Wild Wild West of Scientific Computing with Student Learners

Last revision: 06/13/2019

### Note

In its current state this replication package does not include any data files as we are waiting to publish the anonymized data until we receive IRB approval.

## Folder Structure

The folder contents are as follows:

### Code To Run Experiment

The code can be found inside the *2019ScientificComputing* folder as it was run on the server. It contains a readme.md file with some information about how the system works and how to set it up. This zip also contains the tasks, instruction sheets (as used in the actual experiment), and the solution scripts that were concatenated to participants code so they could be checked.

### Corrections
The paper mentions issues with some of the tasks. The *corrections* folder includes the corrected instruction sheets.

### Data Analysis Code
The folder *dataAnalysis* contains the following files:

* DataAnalysis.r - this script organizes the cleaned data and creates the graph "ComparisonofTaskCompletionTimes.eps". It also calculates several descriptive statistics.
* finalCodeClean.r - this code cleans the data file allData.csv as much as possible. (NOTE: some manual coding is needed to create the completely cleaned csv)
* QualitativeReview.csv - this is the qualitative data that has been anonymized and coded.
* RandomSampleandCalculationsforKappa.xlsx - this is the spreadsheet for the Kappa calculations.

The folder also contains the *quantitativeAnalysisReplication* folder, which contains code to perform the analysis of quantitative errors (interpreter errors). It contains the following files:

* analysis.r - The R script to perform the final evaluation
* run.sh - A bash script that creates the needed folders and runs the python scripts in sequence.
* clean.sh - A bash script to help with automatic cleanup of the work area
* errorclass.py - A python script that classifies the different error messages
* joinqual.py - A python script that joins the quantitative error CSV file with the qualitative error CSV file
* eval_join.py - A python script that outputs a number of different data tables and a correlation matrix graph, as well as a long form CSV file of the joined data
* qual.csv - A CSV file containing the qualitative data
* events.csv - A CSV file containing data from each compilation event.

## Graphs
The *graphs* folder contains the full size EPS graphs used in the paper.

## Task Information
The *tasksInstructionsandSolutions* folder contains the tasks and the solutions to the tasks

