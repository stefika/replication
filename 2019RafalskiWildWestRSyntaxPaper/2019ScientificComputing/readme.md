# README

## Installation

* Install and set-up Apache and PHP 
  - [helpful] (https://coolestguidesontheplanet.com/get-apache-mysql-php-and-phpmyadmin-working-on-osx-10-11-el-capitan/)
* Install MySQL
  - Remember Admin password
  - change Admin password using `mysql -u root -p` and `SET PASSWORD
    = PASSWORD('newpw');`
* Install PhpMyAdmin
  - follow instructions from guide
* initialize database as defined in code/config.php using the sql files in code/sql by creating database `CREATE DATABASE databasename` and then paste the syn.sql file into the sql console in phpmyadmin
* create user as defined in code/config.php `CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';`
* give user rights `GRANT ALL PRIVILEGES ON databasename . * TO 'newuser'@'localhost';`
* create tmp/ folder in EPI/
* use 'fixrights.sh' file to fix the rights on the tmp/ folder.

### index.php

Index screen

Info + insert email and press button to continue
button goes to code/validate_email.php
Validates email and goes to informed_consent.php

### informed_consent.php

Has the informed consent info.

Had accept and decline button

decline leads to code/decline_consent.php which leads to declined.php

accept leads to code/accept_consent.php which inserts the
accept event into db. Then leads to survey_1.php.

### survey_1.php

Has the main survey about participant demographics.
Submit leads to code/validate_page1.php, which valudates the survey data and
inputs it into the database. 

This then leads to experiment_protocol.php.

### experiment_protocol.php

Has information on the experiment protocol with button to lead to next page
when read.

The button leads to code/begin_tasks.php. That uses code/studyConfig.json to
retrieve info on the tasks. This then loads tasks.php.

### tasks.php

Loads in tasks using JavaScript ajax commands from the code/js/tasks.js file.

A click on the submit button changes the task to the next until no more tasks
exist. Then, the JavaScript file will load the survey_2.php file.

### survey_2.php

Has additional demoraphics info such as gender and age and programming
experience.

The submit button of this page loads code/validate_page2.php.

That file validates and saves the info from the survey into the DB.

After that the feedback.php site is loaded.

### feedback.php

Asks questions about what would have made this experiment better.

Submit button saves results using ajax call to code/validate_feedback.php and goes to finished.php.

### finished.php

Thanks the participant for taking part and tells them they are done.
