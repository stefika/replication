#!/usr/bin/Rscript
# This code loads the database so that work can be done

# setwd("/Users/applephox/repositories/masters/RSyntax/experimentScripts")
# americanStats<-read.csv("americanStats.csv", header=TRUE)

# For each tasks 1 to 6 there exists a table called americanStats that looks like this:

#    playerID 	yearID 	teamID lgID   G  AB   R   H HR 	RBI SB
#1  abreujo02   2017    CHA   	AL 	156 621  95 189 33 	102  3
#2  altuvjo01   2017    HOU   	AL 	153 590 112 204 24  81 	32
#3  anderti01   2017    CHA   	AL 	146 587  72 151 17  56 	15
#4  andruel01   2017    TEX   	AL 	158 643 100 191 20  88 	25
#5  aokino01   	2017    HOU   	AL	 71 202  28  55  2  19   5
#6  barneda01   2017    TOR   	AL 	129 336  34  78  6  25   7
#7  bautijo02   2017    TOR   	AL 	157 587  92 119 23  65   6
#8  beninan01   2017    BOS   	AL 	151 573  84 155 20  90 	20

# Using the sample section on the left as a guide, you are to 
# write a line of R code to use the transform() method on the 
# table americanStats to divide the column variable HR by the column 
# variable AB and store it in a new column variable called HRtoABRatio.

# Type your code here 

nonZeroentry = "Error" 

solution = transform(americanStats, HRtoABRatio = americanStats$HR / americanStats$AB)

identical(YourAnswerIs, solution)