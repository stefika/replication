#!/bin/python3

import pandas as pd

#df_qual = pd.read_csv("qual.csv")
df_ground_truth = pd.read_csv("qual.csv")
df_quant = pd.read_csv("quant.csv")

# different column names require lowercase
df_ground_truth = df_ground_truth.rename(str.lower, axis='columns')

# don't need these columns for analysis and they don't combine well
df_quant = df_quant.drop(columns=['output', 'entry', 'time'])

# filter out incorrect answers
df_quant = df_quant[df_quant['error_class'] != "incorrect answer"]

#
# make wide matrix-style columns for errors
#
# first drop the columns from table
drop_columns = ['error_class', 'error_category']
df_quant_dropped = df_quant.drop(drop_columns, 1)

# then create df with dummies (matrix style)
df_quant_dummies = pd.get_dummies(df_quant.error_class).mul(1)

# concat the tables
matrix = pd.concat([df_quant_dropped, df_quant_dummies], axis=1)


# group into a table with id/task as key
columns = matrix.loc[:, 'argument empty':'wrong use of tilde operator (must have both sides)'].columns
grouped = matrix.groupby(['id','task'])[columns].sum()

# merge everything together
#merged_qual = pd.merge(df_ground_truth ,df_qual, how='left', on=['id', 'task'])
merged = pd.merge(df_ground_truth, grouped, how='left', on=['id', 'task'])
#merged = pd.merge(df_qual, grouped, how='left', on=['id', 'task'])

# because we do a left join on the qual data we need to fill in the empties
merged = merged.fillna(0)

merged['sum_errors'] = merged[columns].apply(lambda x: sum(x), axis=1)

#print(df_ground_truth.shape, grouped.shape, merged.shape)
#print(df_qual.shape, grouped.shape, merged.shape)

merged.to_csv('joined.csv', index=False)
