#!/bin/bash
mkdir output
mkdir graphs
echo "Folders created"
python3 errorclass.py
echo "Errors Classified"
python3 joinqual.py
echo "Qual and Quant joined"
python3 eval_join.py
echo "Long joined file and analysis files created"
