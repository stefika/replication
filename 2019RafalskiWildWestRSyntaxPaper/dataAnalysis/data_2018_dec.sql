select 
		`A`.`id` AS `id`,
		`A`.`year` AS `year`,
		`A`.`lang` AS `lang`,
		`A`.`task` AS `task`,
		`A`.`timeBegin` AS `timeBegin`,
		`B`.`timeEnd` AS `timeEnd`,
		timestampdiff(SECOND,`A`.`timeBegin`,`B`.`timeEnd`) AS `Time` 
		from (
				((select `events`.`id` AS `id`,
						`p1`.`lang` AS `lang`,
						`survey`.`year` AS `year`,
						min(`events`.`time`) AS `timeBegin`,
						`events`.`task` AS `task`
					from (
						(`events` 
						join `survey` on((`events`.`id` = `survey`.`id`))) 
						join `participants` `p1` on ((`p1`.`id` = `events`.`id`))) 
					where (`events`.`event` = 6 ) 
					group by `survey`.`year`,`events`.`id`,`events`.`task`)) `A` 
				
				join 
			    (select `events`.`id` AS `id`,
			    		`p2`.`lang` AS `lang`,
			    		`survey`.`year` AS `year`,
			    		min(`events`.`time`) AS `timeEnd`,
			    		`events`.`task` AS `task`
			    	from ((`events` 
			    		join `survey` on((`events`.`id` = `survey`.`id`))) 
			    	    join `participants` `p2` on((`p2`.`id` = `events`.`id`)))
			    	where ((`events`.`event` = 8) or ((`events`.`event` = 13)) )
			    	group by `survey`.`year`,`events`.`id`,`events`.`task`) `B` 

			on(((`A`.`year` = `B`.`year`) and (`A`.`id` = `B`.`id`) and (`A`.`task` = `B`.`task`)))) 
		where A.id > 17
		group by `A`.`id`,`A`.`task`,`A`.`year`,`A`.`lang`;


		-- or (`events`.`event` = 13)

SELECT `allData1`.`id`,
		`allData1`.`lang` AS assignedStyle, 
		`allData1`.`task`, 
		`allData1`.`Time`, 
		`events`.`eventid` as identifier, 
		`events`.`event` as codeEvent, 
		`events`.`entry`, 
		`events`.`output`
From ((`allData1`JOIN `events` on (`allData1`.`id` = `events`.`id` and `allData1`.`task` = `events`.`task`)) 
Where  
((`events`.`event` = 8) OR (`events`.`event` = 13) OR (`events`.`event` = 22)) 



SELECT `allData1`.`id`,
		`allData1`.`lang` AS assignedStyle, 
		`allData1`.`task`, 
		`allData1`.`Time`, 
		`events`.`eventid` as identifier, 
		`events`.`event` as codeEvent, 
		`events`.`entry`, 
		`events`.`output`, 
		`survey`.`year`,
		`survey`.`totalxp`, 
		`survey`.`major`, 
		`survey`.`language` AS spokenLanguage, 
		`survey`.`gender`, 
		`survey`.`education` 
From ((`allData1`JOIN `events` on (`allData1`.`id` = `events`.`id` and `allData1`.`task` = `events`.`task`)) 
      JOIN `survey`on (`allData1`.`id` = `survey`.`id`))
Where  
((`events`.`event` = 8) OR (`events`.`event` = 13) OR (`events`.`event` = 22)) 


SELECT `allData1`.`id`,
		`allData1`.`lang` AS assignedStyle, 
		`allData1`.`task`, 
		`allData1`.`Time`, 
		`events`.`eventid` as identifier, 
		`events`.`event` as codeEvent, 
		`events`.`entry`, 
		`events`.`output`, 
		`participants`.`courseNum`
From ((`allData1`JOIN `events` on (`allData1`.`id` = `events`.`id` and `allData1`.`task` = `events`.`task`)) 
      JOIN `participants` on (`allData1`.`id` = `participants`.`id` and `allData1`.`lang` = `participants`.`lang`))
Where  
((`events`.`event` = 8) OR (`events`.`event` = 13) OR (`events`.`event` = 22)) 
AND `participants`.`valid` = 1

SELECT `allData1`.`id`,
		`allData1`.`lang` AS assignedStyle, 
		`allData1`.`task`, 
		`allData1`.`Time`, 
		`events`.`eventid` as identifier, 
		`events`.`event` as codeEvent, 
		`events`.`entry`, 
		`events`.`output`, 
		`participants`.`courseNum`,
		`survey`.`year`,
		`survey`.`totalxp`, 
		`survey`.`major`, 
		`survey`.`language` AS spokenLanguage, 
		`survey`.`gender`, 
		`survey`.`education` 
From (((`allData1`JOIN `events` on (`allData1`.`id` = `events`.`id` and `allData1`.`task` = `events`.`task`)) 
      JOIN `participants` on (`allData1`.`id` = `participants`.`id` and `allData1`.`lang` = `participants`.`lang`))
      JOIN `survey`on (`allData1`.`id` = `survey`.`id`))
Where  
((`events`.`event` = 8) OR (`events`.`event` = 13) OR (`events`.`event` = 22)) 
AND `participants`.`valid` = 1




SELECT * FROM `totalDB`, `allData` WHERE `totalDB`.`id` = `allData`.`id` GROUP by `totalDB`.`id`, `totalDB`.`task`, `allData`.`codeEvent`, `allData`.`entry`, `allData`.`output`






select * from `allData1`JOIN `events` on (`allData1`.`id` = `events`.`id` and `allData1`.`task` = `events`.`task`) WHERE ((`events`.`event` = 8) or ((`events`.`event` = 13)) )





