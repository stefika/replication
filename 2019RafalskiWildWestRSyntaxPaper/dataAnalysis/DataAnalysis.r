install.packages(c("tidyverse", "stringr", "car", "compute.es", "Hmisc", "ez", "multcomp", "pastecs", "WRS2"))
library(car)
library(compute.es)
library(multcomp)
library(pastecs)
library(WRS2)
library(ez)
library(Hmisc)
library(tidyverse)

#home dir
setwd("E:\\repositories\\masters\\RSyntax\\scripts")
#laptop dir
# setwd("C:\\Users\\fauxphox\\Documents\\repositories\\masters\\RSyntax\\scripts")
#lab dir
#setwd("~/repositories/masters/RSyntax/scripts")

# completed the tasks work
# read the csv
# bring in new cleaned csv ready for analysis

masterData<-read.csv("dataCleanedStep6.csv")

# manual removal of id 36  from dataCleanedStep6.csv was missed so we need to remove it here. 
masterData<-masterData%>%filter(id!=36)

#ezDesign(data = masterData, id, task)
# get incorrect majors from each table and swap them
getNonCsMajor<-masterData%>%filter(id ==64 | id==190)
getCsMajor<-masterData%>%filter(id==152 | id == 175)
# remove them from the master data set
masterData<-masterData%>%filter(id!=64)
masterData<-masterData%>%filter(id!=152)
masterData<-masterData%>%filter(id!=175)
masterData<-masterData%>%filter(id!=190)

# group by major
groupByMajorCs<-masterData%>%filter((str_detect(major, "Computer Science")) | (str_detect(major, "CS"))  | (str_detect(major, "cs"))
                                     | (str_detect(major, "COMPUTER SCIENCE")) 
                                     | (str_detect(major, "Computer Engineering"))
                                     | (str_detect(major, "computer science"))
                                     | (str_detect(major, "CPE"))
                                     | (str_detect(major, "cpe")) | (str_detect(major, "Computer science now"))
)

# create CS major table
groupByMajorCs<-rbind(groupByMajorCs,getCsMajor)
csMajorDemographic<-distinct(groupByMajorCs,id, major)
csMajorTibble<-as_tibble(groupByMajorCs)
csMajor<-csMajorTibble%>%select(id, task, assignedStyle, gender, Time)
csMajor<-add_column(csMajor, groupedMajor="CS")

# create nonCS major table
groupByMajorNonCs<-masterData%>%filter((str_detect(major, 'Computer Science')==FALSE)
                                        & (str_detect(major, "CS")==FALSE)
                                        & (str_detect(major, "cs")==FALSE)
                                        & (str_detect(major, "COMPUTER SCIENCE")==FALSE) 
                                        & (str_detect(major, "Computer Engineering")==FALSE)
                                        & (str_detect(major, "computer science")==FALSE)
                                        & (str_detect(major, "CPE")==FALSE)
                                        & (str_detect(major, "cpe")==FALSE)
                                        & (str_detect(major, "Computer science now")==FALSE)
)
groupByMajorNonCs<-rbind(groupByMajorNonCs,getNonCsMajor)
groupByMajorNonCs$assignedStyle<-as.factor(groupByMajorNonCs$assignedStyle)
NoncsMajorDemographic<-distinct(groupByMajorNonCs,id, major)
nonCsMajorTibble<-as.tibble(groupByMajorNonCs)
nonCsMajor<-nonCsMajorTibble%>%select(id, task, assignedStyle, gender, Time)
nonCsMajor<-add_column(nonCsMajor, groupedMajor="Other")

#concatinate noncs and cs and set id, major, and task as factors
groupByMajor<-rbind(csMajor, nonCsMajor)

groupByMajor$id<-as.factor(groupByMajor$id)
groupByMajor$assignedStyle<-as.factor(groupByMajor$assignedStyle)
groupByMajor$task<-as.factor(groupByMajor$task)
groupByMajor$groupedMajor<-as.factor(groupByMajor$groupedMajor)


#write_csv(groupByMajor, "validIdAndTaskWithReplacement.csv")

##############################################################################################
# count by different demographic
# group rows by distinct id
# bring in new cleaned csv ready for analysis

masterData<-read.csv("dataCleanedStep6.csv")

# manual removal of id 36  from dataCleanedStep6.csv was missed so we need to remove it here. 
masterData<-masterData%>%filter(id!=36)

uniqueId<-distinct(masterData, id, assignedStyle, gender, major, year, education, spokenLanguage, totalxp)

rcount<-uniqueId%>%filter(assignedStyle==1)%>%count()
Tildecount<-uniqueId%>%filter(assignedStyle==2)%>%count()
Tidy<-uniqueId%>%filter(assignedStyle==3)%>%count()

counts<-c(rcount, Tildecount, Tidy)


cSCount<-uniqueId%>%filter((str_detect(major, 'Computer Science')) 
                                   | (str_detect(major, "CS"))
                                   | (str_detect(major, "cs")) 
                                   | (str_detect(major, "COMPUTER SCIENCE")) 
                                   | (str_detect(major, "Computer Engineering"))
                                   | (str_detect(major, "computer science"))
                                   | (str_detect(major, "CPE"))
                                   | (str_detect(major, "cpe"))
                                   | (str_detect(major, "Computer science now"))
)%>%count()

OtherCount<-uniqueId%>%filter((str_detect(major, 'Computer Science')==FALSE)
                              & (str_detect(major, "CS")==FALSE)
                              & (str_detect(major, "cs")==FALSE) 
                              & (str_detect(major, "COMPUTER SCIENCE")==FALSE) 
                              & (str_detect(major, "Computer Engineering")==FALSE)
                              & (str_detect(major, "computer science")==FALSE)
                              & (str_detect(major, "CPE")==FALSE)
                              & (str_detect(major, "cpe")==FALSE)
                              & (str_detect(major, "Computer science now")==FALSE)
)%>%count()

#Gender
FemaleCount <- uniqueId%>%filter(str_detect(gender, 'Male')==FALSE)%>%count()
maleCount <- uniqueId%>%filter(str_detect(gender, 'Male'))%>%count()

#language
englishCount <- uniqueId%>%filter(str_detect(spokenLanguage, 'English'))%>%count()

#programming experience
noXP<- uniqueId%>%filter(totalxp < 1)%>%count()
oneXP<- uniqueId%>%filter(totalxp == 1)%>%count()

#twoOrMoreXP<- uniqueId%>%filter(((str_detect(totalxp, '0')==FALSE) | (str_detect(totalxp, '1')==FALSE)))%>%count()

#year                             
groupByFreshman<-uniqueId%>%filter(str_detect(year, 'Freshman'))%>%count()  

groupBySophomore<-uniqueId%>%filter(str_detect(year, 'Sophomore'))%>%count()  

groupByJunior<-uniqueId%>%filter(str_detect(year, 'Junior'))%>%count()  

groupBySenior<-uniqueId%>%filter(str_detect(year, 'Senior'))%>%count()  

groupByGradeLevelGraduatePlus<-uniqueId%>%filter((str_detect(year, 'Freshman')==FALSE)
                                               & (str_detect(year, 'Sophomore')==FALSE)
                                               & (str_detect(year, 'Junior')==FALSE)
                                               & (str_detect(year, 'Senior')==FALSE)
)

groupByGraduate<-groupByGradeLevelGraduatePlus%>%filter((str_detect(year, 'Graduate'))
                                                        | (str_detect(year, 'Post-graduate')))
                             
#uniqueId%>%filter(str_detect(education, 'Masters Degree'))

#Education
someEd<- uniqueId%>%filter(str_detect(education, 'Some college / university'))%>%count()
bachEd<- uniqueId%>%filter(str_detect(education, 'Bachelor Degree'))%>%count()
assocEd<- uniqueId%>%filter(str_detect(education, 'Associate Degree'))%>%count()
highEd<- uniqueId%>%filter(str_detect(education, 'High School / GED'))%>%count()

moreThanBach<-uniqueId%>%filter((str_detect(education, 'Some college / university')== FALSE) &
                                (str_detect(education, 'Bachelor Degree')== FALSE) &
                                (str_detect(education, 'Associate Degree')== FALSE) &
                                (str_detect(education, 'High School / GED')== FALSE))



######################################################################################################
# Facet grid calculations



######################################################################################################
levels(groupByMajor$assignedStyle)<-c("base R","tilde","tidyverse")
levels(groupByMajor$task)<-c(1,2,4,5,6,8,9,10)

ggplot(groupByMajor, aes(groupedMajor,Time,fill=groupedMajor)) +
geom_boxplot() + facet_grid(rows=vars(assignedStyle), cols = vars(task)) + labs(x = "Major", y = "Completion Time", fill="Major") + 
  scale_fill_grey(start=0.6, end=0.9) +
  theme(legend.position="bottom",text = element_text(size=24)) + 
  scale_x_discrete(labels=c("1" = "rBasic", "2" = "Tilda","3" = "Tidy"))

ggsave("graphs/facetGridByMajorTaskLang.eps" )


######################################################################################################
# ANOVA test 

newModel<-ezANOVA(data = groupByMajor,
                  dv = .(Time),
                  wid = .(id),
                  within = .(task),
                  between = .(assignedStyle, groupedMajor),
                  detailed = TRUE,
                  type = 3)

newModel

describe(groupByMajor)

############################################################################################################
#descriptive statistics

timeMeanOverall<-mean(groupByMajor$Time)

timeMeanStyle<-aggregate(groupByMajor$Time,by = list(style = groupByMajor$assignedStyle),mean)

timeMeanTask<-aggregate(groupByMajor$Time,by = list(task=groupByMajor$task, style = groupByMajor$assignedStyle),mean)

timeMeanMajor<-aggregate(groupByMajor$Time,by = list(time = groupByMajor$groupedMajor ),mean)

timeSdOverall<-sd(groupByMajor$Time)

timeSdStyle<-aggregate(groupByMajor$Time,by = list(style = groupByMajor$assignedStyle),sd)

timeSdTask<-aggregate(groupByMajor$Time,by = list(task=groupByMajor$task, style = groupByMajor$assignedStyle),sd)

timeSdMajor<-aggregate(groupByMajor$Time,by = list(time = groupByMajor$groupedMajor ),sd)

timeMean<-c(timeMeanOverall,timeMeanStyle,timeMeanMajor,timeMeanTask)

timeSd<-c(timeSdOverall,timeSdStyle,timeSdMajor,timeSdTask)
